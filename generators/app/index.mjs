/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import chalk from "chalk";
import {GeneratorBaseEntities} from 'generator-jhipster'
import {PRIORITY_PREFIX, PROMPTING_PRIORITY, COMPOSING_PRIORITY} from 'generator-jhipster/esm/priorities';
import {blueprints, DEVOPS_BLUEPRINT, ENTITY_AUTO_ROLE, IDENTIFIER_CONVERTER} from '../constants.mjs'

export default class extends GeneratorBaseEntities {

    constructor(args, opts, features) {
        super(args, opts, {taskPrefix: PRIORITY_PREFIX, unique: 'namespace', ...features});

        this.jhipsterOptions({
            executeBlueprint: {
                desc: 'Sub Blueprint',
                type: String,
                scope: 'blueprint',
            }
        });

        if (this.options.help) return;

        if (!this.options.jhipsterContext) {
            throw new Error(`This is a JHipster blueprint and should be used only like ${chalk.yellow('jhipster --blueprints entity-audit')}`);
        }

        this.sbsBlueprint = true;
    }

    /**
     * @returns {{promptingTemplateAsync(): Promise<void>}}
     */
    get [PROMPTING_PRIORITY]() {
        return {
            async promptingTemplateAsync() {
                let chooseRes = await this.prompt([
                    {
                        type: 'list',
                        name: 'executeBlueprint',
                        message: 'Which blueprint do you want to use?',
                        choices: blueprints
                    }
                ]);
                this.executeBlueprint = chooseRes.executeBlueprint;
            }
        }
    }

    get [COMPOSING_PRIORITY]() {
        return {
            async composingTask() {
                if (this.executeBlueprint === ENTITY_AUTO_ROLE) {
                    await this.composeWithJHipster(`${DEVOPS_BLUEPRINT}:${ENTITY_AUTO_ROLE}`);
                } else if (this.executeBlueprint === IDENTIFIER_CONVERTER) {
                    await this.composeWithJHipster(`${DEVOPS_BLUEPRINT}:${IDENTIFIER_CONVERTER}`);
                }
            }
        }
    }

}
