/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { SECURITY_PLUGIN_VERSION } from '../constants.mjs';

const SECURITY_PROPERTY = `entity-auto-role-plugin.version`;

export const property = {
    name: SECURITY_PROPERTY,
    value: SECURITY_PLUGIN_VERSION
};

export const dependencies = [
    {
        groupId: "uz.devops.khasanof",
        artifactId: "entity-auto-role-plugin",
        version: `$\{${SECURITY_PROPERTY}}`
    },
    {
        groupId: "org.springframework.boot",
        artifactId: "spring-boot-starter-oauth2-client",
        version: "2.7.14"
    },
    {
        groupId: "org.springframework.security",
        artifactId: "spring-security-oauth2-jose",
        version: "5.7.10"
    },
    {
        groupId: "org.springframework.security",
        artifactId: "spring-security-oauth2-resource-server",
        version: "5.7.10"
    },
    {
        groupId: "org.reflections",
        artifactId: "reflections",
        version: "0.10.2"
    },
];

export const mavenPlugin= (basedir) => {
    return {
        groupId: "org.apache.maven.plugins",
        artifactId: "maven-install-plugin",
        version: "2.5.2",
        other: `<executions>
                    <execution>
                        <id>install-entity-auto-role-plugin</id>
                        <phase>clean</phase>
                        <goals>
                            <goal>install-file</goal>
                        </goals>
                        <configuration>
                            <file>${basedir}/lib/entity-auto-role-plugin-$\{${SECURITY_PROPERTY}}.jar</file>
                            <repositoryLayout>default</repositoryLayout>
                            <groupId>uz.devops.khasanof</groupId>
                            <artifactId>entity-auto-role-plugin</artifactId>
                            <version>$\{${SECURITY_PROPERTY}}</version>
                            <packaging>jar</packaging>
                            <generatePom>true</generatePom>
                        </configuration>
                    </execution>
                </executions>`
    }
}
