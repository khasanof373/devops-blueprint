/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {chooseLangPrompt, chooseUpdateType, operationPrompt} from "./prompt.mjs";
import {chooseEntitiesPrompt} from '../prompt.mjs';
import chalk from "chalk";
import {addEntityAutoRoleMaven, entitiesWriter} from "./utils.mjs";
import {takeMatchEntities} from '../utils.mjs';
import {SECURITY_PLUGIN_VERSION} from '../constants.mjs'
import {files} from "./file.mjs";

export async function promptingFunction() {
    let chooseLang = await chooseLangPrompt(this); // currently not in use!
    let operation = await operationPrompt(this); // currently not in use!
    let updateTypeRes = await chooseUpdateType(this);

    if (updateTypeRes.updateType === 'all') {
        this.roleSetEntities = this.getExistingEntities().map(e => e.name);
    } else if (updateTypeRes.updateType === 'annotated') {
        this.roleSetEntities = this.enableAnnEntities.map(e => e.name);
    } else {
        let response = await chooseEntitiesPrompt(this);
        this.roleSetEntities = response.chooseEntities;
    }
}

export async function writingFilesFunction(application, entities) {
    console.log(`${chalk.red('WRITING_ENTITIES_PRIORITY start')}`)
    const {packageFolder} = application;
    console.log(`SRC DIR : ${packageFolder}`)
    let stringArray = entitiesWriter(takeMatchEntities(entities, this.roleSetEntities));
    // let stringArray = entitiesWriterV2(packageName, this.roleSetEntities);
    application.entitiesValue = stringArray[0];
    application.entitiesImport = stringArray[1];
    await this.writeFiles({blocks: files(packageFolder), context: {...application}});
}

export async function writingPomFunction() {
    console.log("start writing with pom.xml")
    await addEntityAutoRoleMaven(SECURITY_PLUGIN_VERSION, "${project.basedir}", this);
}

export function initializingEntitiesFunction() {
    this.info(`${chalk.green('INITIALIZING_PRIORITY start!')}`)
    let entities = this.getExistingEntities();
    let enableEntities = [];
    for (let i = 0; i < entities.length; i++) {
        if (entities[i].definition.enableAutoRole) {
            let entity = {
                "name": entities[i].name,
                "enableAutoRole": entities[i].definition.enableAutoRole
            };
            // entity.name = entities[i].name;
            // entity.enableAutoRole = entities[i].definition.enableAutoRole;
            enableEntities.push(entity)
        }
    }
    this.enableAnnEntities = enableEntities;
}
