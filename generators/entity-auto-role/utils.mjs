/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { dependencies, property, mavenPlugin } from './constants.mjs';

export function entitiesWriter(entities) {
    let entitiesValue = "";
    let entitiesImport = "";
    let count = 0;
    for (let i = 0; i < entities.length; i++) {
        if ((entities.length - count) === 1) {
            entitiesImport = entitiesImport.concat(`import ${entities[i].packageName}.domain.${entities[i].persistClass};`)
            entitiesValue = entitiesValue.concat(`new EntityAuthorize(${entities[i].persistClass}.class, true)`);
        } else {
            entitiesImport = entitiesImport.concat(`import ${entities[i].packageName}.domain.${entities[i].persistClass}; \n`);
            entitiesValue = entitiesValue.concat(`new EntityAuthorize(${entities[i].persistClass}.class, true), \n`);
            count++;
        }
    }
    return [entitiesValue, entitiesImport];
}

export function entitiesWriterV2(packageName, entities) {
    let entitiesValue = "";
    let entitiesImport = "";
    let count = 0;
    for (let i = 0; i < entities.length; i++) {
        if ((entities.length - count) === 1) {
            entitiesImport = entitiesImport.concat(`import ${packageName}.domain.${entities[i]};`)
            entitiesValue = entitiesValue.concat(`new EntityAuthorize(${entities[i]}.class, true)`);
        } else {
            entitiesImport = entitiesImport.concat(`import ${packageName}.domain.${entities[i]}; \n`);
            entitiesValue = entitiesValue.concat(`new EntityAuthorize(${entities[i]}.class, true), \n`);
            count++;
        }
    }
    return [entitiesValue, entitiesImport];
}

export function addEntityAutoRoleMaven(autoRoleVersion, basedir, generator) {
    generator.addMavenProperty(property.name, property.value);
    dependencies.forEach(dependency => {
        generator.addMavenDependency(dependency.groupId, dependency.artifactId, dependency.version);
    })
    let plugin = mavenPlugin(basedir);
    generator.addMavenPlugin(plugin.groupId, plugin.artifactId, plugin.version, plugin.other);
}

export function takeMatchEntities(entities, selectedEntities) {
    return entities.filter((f) => selectedEntities.includes(f.name));
}
