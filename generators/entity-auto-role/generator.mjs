/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import chalk from 'chalk';
import { GeneratorBaseEntities } from 'generator-jhipster';
import {
    initializingEntitiesFunction,
    promptingFunction,
    writingFilesFunction,
    writingPomFunction
} from './generator-service.mjs';
import {
    CONFIGURING_EACH_ENTITY_PRIORITY,
    DEFAULT_PRIORITY,
    END_PRIORITY,
    INITIALIZING_PRIORITY,
    PRIORITY_PREFIX,
    PROMPTING_PRIORITY,
    WRITING_ENTITIES_PRIORITY,
} from 'generator-jhipster/esm/priorities';

export default class extends GeneratorBaseEntities {

    constructor(args, opts, features) {
        super(args, opts, {taskPrefix: PRIORITY_PREFIX, unique: 'namespace', ...features});

        this.jhipsterOptions({
            autoRoleLang: {
                desc: 'Entity Auto Role Blueprint',
                type: String,
                scope: 'blueprint'
            },
            roleSetEntities: {
                desc: 'Auto Role Entities',
                type: Array
            },
            enableAnnEntities: {
                desc: 'EnableAutoRole annotation has entities',
                type: Array
            },
            applicationConfig: {
                desc: 'Application Configs',
                type: Object
            },
            applicationEntities: {
                desc: 'Application Entities',
                type: Array
            }
        })

        if (this.options.help) return;

        if (!this.options.jhipsterContext) {
            throw new Error(`This is a JHipster blueprint and should be used only like ${chalk.yellow('jhipster --blueprints bptest')}`);
        }
    }

    async _postConstruct() {
        await this.dependsOnJHipster("bootstrap-application");
    }

    get [INITIALIZING_PRIORITY]() {
        return {
            async initializingTemplateTask() {
                initializingEntitiesFunction.call(this);
            },
        };
    }

    get [PROMPTING_PRIORITY]() {
        return {
            async promptingTemplateTask() {
                await promptingFunction.call(this);
            },
        };
    }

    get [WRITING_ENTITIES_PRIORITY]() {
        return {
            async writingPomTask() {
                await writingPomFunction.call(this);
            },
            async writingTemplateTask({ application, entities }) {
                await writingFilesFunction.call(this, application, entities);
            }
        }
    }

    get [END_PRIORITY]() {
        return {
            async endTemplateTask() {
                this.log(`${chalk.yellowBright('EntityAutoRole for JHipster App created successfully! 🎉')}`)
            },
        };
    }
}

