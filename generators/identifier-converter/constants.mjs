/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

export const uuid = '00000000-0000-0000-0000-00000000000';

const longType = /AtomicLong count = new AtomicLong(.*);/gm
const intType = /AtomicInteger count = new AtomicInteger(.*);/gm
const stringType = /String count = RandomStringUtils\.random(.*);/
const uuidType = /UUID count = UUID\.randomUUID\(\);'/gm

const uuidValue = `UUID count = UUID.randomUUID();`;
const stringValue = 'String count = RandomStringUtils.random(12);';
const intValue = 'AtomicInteger count = new AtomicInteger(random.nextInt(Integer.MAX_VALUE));';
const longValue = 'AtomicLong count = new AtomicLong(random.nextLong(Long.MAX_VALUE));';

const defaultLong = '1L';
const defaultInt = '1';
const defaultString = '"DEVOPS"';
const defaultUuid = `UUID.fromString("${uuid}1")`;

const getMethodLongReg = /\.getId\(\).intValue\(\)/gm;
const getMethodIntReg = /\.getId\(\)/gm;
const getMethodStringReg = /\.getId\(\)/gm;
const getMethodsUuidReg = /\.getId\(\).toString\(\)/gm;

const getMethodLong = `.getId().intValue()`;
const getMethodInt = `.getId()`;
const getMethodString = `.getId()`;
const getMethodUuid = `.getId().toString()`;

const setMethodLongReg = /\.setId\(count.incrementAndGet\(\)\)/gm;
const setMethodIntReg = /\.setId\(count.incrementAndGet\(\)\)/gm;
const setMethodStringReg = /\.setId\(count\)/gm;
const setMethodUuidReg = /\.setId\(count\)/gm;

const setMethodLong = '.setId(count.incrementAndGet())';
const setMethodInt = '.setId(count.incrementAndGet())';
const setMethodString = '.setId(count)';
const setMethodUuid = '.setId(count)';

const countLongReg = /ID, count\.incrementAndGet\(\)/gm;
const countIntReg = /ID, count\.incrementAndGet\(\)/gm;
const countStringReg = /ID, count/gm;
const countUuidReg = /ID, count/gm;

const countLong = 'ID, count.incrementAndGet()';
const countInt = 'ID, count.incrementAndGet()';
const countString = 'ID, count';
const countUuid = 'ID, count';

export const getType = (type) => {
    if (type === 'UUID') return uuidType;
    else if (type === 'Integer') return intType;
    else if (type === 'String') return stringType;
    else return longType;
}

export const getValue = (convertType) => {
    if (convertType === 'UUID') return uuidValue;
    else if (convertType === 'Integer') return intValue;
    else if (convertType === 'String') return stringValue;
    else return longValue;
}

export const getDefaultValue = (type) => {
    if (type === 'UUID') return defaultUuid;
    else if (type === 'Integer') return defaultInt;
    else if (type === 'String') return defaultString;
    else return defaultLong;
}

export const getMethodReg = (type) => {
    if (type === 'UUID') return getMethodsUuidReg;
    else if (type === 'Integer') return getMethodIntReg;
    else if (type === 'String') return getMethodStringReg;
    else return getMethodLongReg;
}

export const getMethod = (type) => {
    if (type === 'UUID') return getMethodUuid;
    else if (type === 'Integer') return getMethodInt;
    else if (type === 'String') return getMethodString;
    else return getMethodLong;
}

export const setMethodReg = (type) => {
    if (type === 'UUID') return setMethodUuidReg;
    else if (type === 'Integer') return setMethodIntReg;
    else if (type === 'String') return setMethodStringReg;
    else return setMethodLongReg;
}

export const setMethod = (type) => {
    if (type === 'UUID') return setMethodUuid;
    else if (type === 'Integer') return setMethodInt;
    else if (type === 'String') return setMethodString;
    else return setMethodLong;
}

export const countMethodReg = (type) => {
    if (type === 'UUID') return countUuidReg;
    else if (type === 'Integer') return countIntReg;
    else if (type === 'String') return countStringReg;
    else return countLongReg;
}

export const countMethod = (type) => {
    if (type === 'UUID') return countUuid;
    else if (type === 'Integer') return countInt;
    else if (type === 'String') return countString;
    else return countLong;
}

export const convertTypes = [
    {
        name: 'String',
        value: 'String'
    },
    {
        name: 'Long',
        value: 'Long',
    },
    {
        name: 'Integer',
        value: 'Integer',
    },
    {
        name: 'UUID',
        value: 'UUID',
    }
]

export const liquibaseTypes = [
    {
        name: 'UUID',
        value: '${uuidType}'
    },
    {
        name: 'String',
        value: 'varchar(255)'
    },
    {
        name: 'Integer',
        value: 'int'
    },
    {
        name: 'Long',
        value: 'bigint'
    }
]

export const liquibaseLoadDataTypes = [
    {
        name: 'UUID',
        value: '${uuidType}'
    },
    {
        name: 'String',
        value: 'string'
    },
    {
        name: 'Integer',
        value: 'numeric'
    },
    {
        name: 'Long',
        value: 'numeric'
    }
]
