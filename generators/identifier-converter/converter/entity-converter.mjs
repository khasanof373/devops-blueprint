/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {entityNeedles} from '../needles.mjs';
import {constants} from 'generator-jhipster';
import {replaceIdType, replaceEntityTests, replaceIdAnnotations} from "../converter.mjs";

const {SERVER_MAIN_SRC_DIR, SERVER_TEST_SRC_DIR} = constants;

export async function convertEntity(generator, application, entity) {
    console.log(entity);
    console.log('convertEntity start');
    const convertType = generator.convertType;
    const {packageFolder, packageName} = application;
    let convertObject = {
        "idType": entity.idType,
        "convertType": convertType
    }
    let entityPath = `${SERVER_MAIN_SRC_DIR}${packageFolder}/domain/${entity.persistClass}.java`;
    const entityTestPath = `${SERVER_TEST_SRC_DIR}${packageFolder}/domain/${entity.persistClass}Test.java`;
    console.log('replaceIdAnnotations start')
    replaceIdAnnotations(generator, entityPath, entity, convertType, packageName);
    console.log('replaceIdType start')
    replaceIdType(generator, entityPath, entityNeedles(entity.idType), convertObject);
    const paths = {
        "filePath": entityTestPath,
        "packageName": `package ${packageName}.domain;`
    }
    if (!generator.isUserEntity(entity.name)) {
        console.log('replaceEntityTests start')
        replaceEntityTests(generator, entity.persistClass, paths, convertType);
    }
    console.log('convertEntity end');
}
