/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {existsSync} from 'fs';
import {uuid} from '../constants.mjs';
import {uuidRegex} from '../needles.mjs';
import {constants} from 'generator-jhipster';
import {
    snakeCase,
    replaceRegexNeedles
} from "../converter.mjs";

const {SERVER_MAIN_RES_DIR} = constants;

export async function convertFakeData(generator, application, entity) {
    const convertType = generator.convertType;
    const entityType = entity.idType;
    let entitySnakeCaseName = snakeCase(entity.name);
    const fakeDataFile = `${SERVER_MAIN_RES_DIR}config/liquibase/fake-data/${entitySnakeCaseName}.csv`;
    if (existsSync(fakeDataFile)) {
        const lineNumbers = [...Array(10).keys()];
        const needles = [];
        lineNumbers.forEach(num => {
            const id = num + 1;
            needles.push({
                regex: getRegex(entityType, id),
                content: getValue(convertType, id)
            });
        });
        replaceRegexNeedles(generator, fakeDataFile, needles);
    }
}

function getRegex(convertType, id) {
    if (convertType === 'UUID') {
        return new RegExp(uuidRegex, 'gm');
    } else {
        return new RegExp(`^${id};`, 'gm')
    }
}

function getValue(convertType, id) {
    if (convertType === 'UUID') {
        return `${uuid.concat(id)};`;
    } else {
        return `${id};`;
    }
}
