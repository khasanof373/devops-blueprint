/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {entityResourceNeedles, entityResourceTestNeedles} from '../needles.mjs';
import {constants} from 'generator-jhipster';
import {addImports, getDefaultValueConvertType, camelize, replaceRegexNeedles, addImportsV2} from '../converter.mjs';

const {SERVER_MAIN_SRC_DIR, SERVER_TEST_SRC_DIR} = constants;

export async function convertResource(generator, application, entity) {
    console.log('convertResource start');
    const convertType = generator.convertType;
    const {packageFolder, packageName} = application;
    const entityIdType = entity.idType;
    const resourcePackage = `package ${packageName}.web.rest;`;
    const resourcePackageRandomString = `package ${packageName}.web.rest;\n import org.apache.commons.lang3.RandomStringUtils;`;
    const entityCamelize = camelize(entity.name);
    const resourcePath = `${SERVER_MAIN_SRC_DIR}${packageFolder}/web/rest/${entity.persistClass}Resource.java`;
    const resourceTestPath = `${SERVER_TEST_SRC_DIR}${packageFolder}/web/rest/${entity.persistClass}ResourceIT.java`;
    console.log("addImports start")
    addImports(generator, resourcePath, resourcePackage, convertType);
    let convertObject = {
        "idType": entityIdType,
        "convertType": convertType
    };
    console.log("replaceIdType start")
    replaceRegexNeedles(generator, resourcePath, entityResourceNeedles(entityIdType, convertType));
    console.log("replaceResourceTests start")
    replaceResourceTests(generator, resourceTestPath, resourcePackage, convertObject, entityCamelize);
    addImportsV2(generator, resourceTestPath, resourcePackage, resourcePackageRandomString, convertType, 'String');
    console.log('convertResource end');
}

function replaceResourceTests(generator, resourceTestPath, resourcePackage, convertObject, entityName) {
    console.log('replaceResourceTests start');
    addImports(generator, resourceTestPath, resourcePackage, convertObject.convertType);
    console.log("addImports end");
    generator.replaceContent(resourceTestPath, getDefaultValueConvertType(convertObject.idType),
        getDefaultValueConvertType(convertObject.convertType), true);
    console.log("replaceContent end");
    replaceRegexNeedles(generator, resourceTestPath, entityResourceTestNeedles(entityName, convertObject.idType, convertObject.convertType));
    console.log('replaceResourceTests end');
}
