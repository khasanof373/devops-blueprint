/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { entityRepositoryNeedles } from '../needles.mjs';
import { addImports , replaceIdType} from '../converter.mjs';
import {constants} from 'generator-jhipster';
const {SERVER_MAIN_SRC_DIR} = constants;

export async function convertRepository(generator, application, entity) {
    console.log("convertRepository start")
    const convertType = generator.convertType;
    const {packageFolder, packageName} = application;
    let convertObject = {
        "idType": entity.idType,
        "convertType": convertType
    }
    const repositoryFilePath = `${SERVER_MAIN_SRC_DIR}${packageFolder}/repository/${entity.persistClass}Repository.java`;
    addImports(generator, repositoryFilePath, `package ${packageName}.repository;`, convertType);
    replaceIdType(generator, repositoryFilePath, entityRepositoryNeedles(entity.idType), convertObject);
    generator.replaceContent(repositoryFilePath, `<${entity.persistClass}, ${entity.idType}>`,
        `<${entity.persistClass}, ${convertType}>`, true);
    console.log("convertRepository end")
}
