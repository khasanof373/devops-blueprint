/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { sync } from 'glob';
import { getRandom } from '../converter.mjs';
import { liquibaseTypes, liquibaseLoadDataTypes } from '../constants.mjs';
import {constants} from "generator-jhipster";
const {SERVER_MAIN_RES_DIR} = constants;
const MAX_VALUE = Number.MAX_SAFE_INTEGER;

export async function convertChangelog(generator, application, entity) {
    console.log('convertChangelog start');
    const convertType = generator.convertType;
    const entityType = entity.idType;
    console.log('convertChangelog sync.call');
    let filePath = `${SERVER_MAIN_RES_DIR}config/liquibase/changelog/${entity.changelogDate}_added_entity_${entity.name}.xml`;
    // generator.replaceContent(file, `id" type="${getType(entityType)}"`, `id" type="${getType(convertType)}"`);
    console.log('convertChangelog replaceContent one');
    generator.replaceContent(filePath, `id" type="${getLoadDataType(entityType)}"`, `id" type="${getLoadDataType(convertType)}"`);
    console.log('convertChangelog replaceContent two');
    generator.replaceContent(filePath, `JHipster will add changesets here -->`,
        changeSet(entity.entityTableName, getType(convertType)));
    console.log('convertChangelog end');
}

const changeSet = (tableName, convertLiquibaseType) => {
    return `JHipster will add changesets here --> \n
            <changeSet author="devops" id="${getRandom(MAX_VALUE)}">
                <modifyDataType
                    columnName="description"
                    newDataType="${convertLiquibaseType}"
                    tableName="${tableName}"/>
            </changeSet>`;
}

function getType(type) {
    return liquibaseTypes.find((o) => o.name === type).value;
}

function getLoadDataType(type) {
    return liquibaseLoadDataTypes.find((o) => o.name === type).value
}
