/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {entityServiceNeedles} from '../needles.mjs';
import {constants} from 'generator-jhipster';
import {addImports, replaceIdType} from '../converter.mjs';

const {SERVER_MAIN_SRC_DIR} = constants;

export async function convertService(generator, application, entity) {
    console.warn(`convertService start ${entity.persistClass}`);
    const convertType = generator.convertType;
    const {packageFolder, packageName} = application;
    const servicePackage = `package ${packageName}.service;`;
    const serviceImplPackage = `package ${packageName}.service.impl;`;
    let hasService = checkService(entity, 'serviceClass'),
        hasImpl = checkService(entity, 'serviceImpl');
    if (hasService || hasImpl) {
        console.log("convertService hasService");
        const servicePath = `${SERVER_MAIN_SRC_DIR}${packageFolder}/service/${entity.persistClass}Service.java`;
        serviceWriter(generator, entity, servicePath, servicePackage, convertType);
    }
    if (hasImpl) {
        console.log("convertService hasImpl");
        const serviceImplPath = `${SERVER_MAIN_SRC_DIR}${packageFolder}/service/impl/${entity.persistClass}ServiceImpl.java`;
        serviceWriter(generator, entity, serviceImplPath, serviceImplPackage, convertType);
    }
    console.log(`convertService end ${entity.persistClass}`);
}

function checkService(entity, val) {
    return entity.service === val;
}

function serviceWriter(generator, entity, path, packageName, convertType) {
    addImports(generator, path, packageName, convertType);
    const convertObject = {
        "idType": entity.idType,
        "convertType": convertType
    };
    replaceIdType(generator, path, entityServiceNeedles(entity.idType), convertObject);
}
