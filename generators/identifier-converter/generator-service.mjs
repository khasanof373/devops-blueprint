/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {takeMatchEntities} from '../utils.mjs';
import {chooseConvertType} from './prompt.mjs';
import {chooseEntitiesPrompt} from '../prompt.mjs';
import { convertEntity } from './converter/entity-converter.mjs';
import { convertChangelog } from './converter/changelog-converter.mjs';
import { convertService } from './converter/service-converter.mjs';
import { convertDto } from './converter/dto-converter.mjs';
import { convertResource } from './converter/resource-converter.mjs';
import { convertFakeData } from './converter/fake-data-converter.mjs';
import { convertRepository } from './converter/repository-converter.mjs';

export async function promptingFunction() {

    let chooseEntitiesRes = await chooseEntitiesPrompt(this);
    let convertTypesRes = await chooseConvertType(this);

    this.domainNames = chooseEntitiesRes.chooseEntities;
    this.convertType = convertTypesRes.convertType;

}

export async function writingFunction(application, entities) {
    console.log('writingFunction start');
    await writeConfiguration(this, application, entities);
    this.modifiedObjects.forEach(entity => {
        converters(this, application, entity);
    })
}

async function converters(generator, application, entity) {
    console.log('converters start');
    await convertEntity(generator, application, entity);
    await convertDto(generator, application, entity);
    await convertService(generator, application, entity);
    await convertRepository(generator, application, entity);
    await convertResource(generator, application, entity);
    await convertChangelog(generator, application, entity);
    await convertFakeData(generator, application, entity);
}

async function writeConfiguration(generator, application, entities) {
    let matchEntities = takeMatchEntities(entities, generator.domainNames);
    let objs = [];
    matchEntities.forEach(entity => {
        let modifiedObject = {
            "name": entity.name,
            "service": entity.service,
            "persistClass": entity.persistClass,
            "entityTableName" : entity.entityTableName,
            "changelogDate" : entity.changelogDate,
            "dto": entity.dto,
            "dtoClass": entity.dtoClass,
            "idType": entity.primaryKey.type,
            "idName": entity.primaryKey.name
        };
        objs.push(modifiedObject);
    });
    generator.modifiedObjects = objs;
}
