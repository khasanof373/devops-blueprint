/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import chalk from 'chalk';
import {GeneratorBaseEntities} from 'generator-jhipster';
import { promptingFunction, writingFunction } from './generator-service.mjs';
import {
    PRIORITY_PREFIX,
    PROMPTING_PRIORITY,
    WRITING_ENTITIES_PRIORITY,
    END_PRIORITY,
} from 'generator-jhipster/esm/priorities';

export default class extends GeneratorBaseEntities {

    constructor(args, opts, features) {
        super(args, opts, {taskPrefix: PRIORITY_PREFIX, unique: 'namespace', ...features});

        this.jhipsterOptions({
            domainNames: {
                desc: 'id is the names of the entity to change',
                type: Array
            },
            converterConfig: {
                desc: 'id converter application configs',
                type: Object
            },
            modifiedObjects: {
                desc: 'entity matching objects',
                type: Array
            },
            convertType: {
                desc: 'entity id convert type',
                type: String
            }
        })

        if (this.options.help) return;

        if (!this.options.jhipsterContext) {
            throw new Error(`This is a JHipster blueprint and should be used only like ${chalk.yellow('jhipster --blueprints bptest')}`);
        }
    }

    async _postConstruct() {
        await this.dependsOnJHipster("bootstrap-application");
    }


    get[PROMPTING_PRIORITY]() {
        return {
            async promptingTask() {
                await promptingFunction.call(this)
            }
        }
    }

    get[WRITING_ENTITIES_PRIORITY]() {
        return {
            async configuringTask({application, entities}) {
                await writingFunction.call(this, application, entities);
            }
        }
    }

    get [END_PRIORITY]() {
        return {
            async endTemplateTask() {
                this.log(`${chalk.yellowBright('IdentifierConverter for JHipster App created successfully! 🎉')}`)
            },
        };
    }

}
