/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {
    getType, getValue, getDefaultValue, getMethodReg,
    getMethod, setMethodReg, setMethod, countMethodReg, countMethod
} from './constants.mjs';

const uuid1 = '00000000-0000-0000-0000-000000000001';
const uuid2 = '00000000-0000-0000-0000-000000000002';

export const entityNeedles = (type) => [
    `private ${type} id`, `id(${type} id) {`,
    `${type} getId() {`, `setId(${type} id) {`
];

export const entityTestNeedles = (entityVarName) => [
    {
        regex: new RegExp(`${entityVarName}1\\.setId\\(1L\\)`, 'gm'),
        content: `${entityVarName}1.setId(UUID.fromString("${uuid1}"))`
    },
    {
        regex: new RegExp(`${entityVarName}2\\.setId\\(2L\\)`, 'gm'),
        content: `${entityVarName}2.setId(UUID.fromString("${uuid2}"))`
    }
];

export const entityResourceNeedles = (type, convertType) => [
    {
        regex: new RegExp(`\\) final ${type} id,`, 'gm'),
        content: `) final ${convertType} id,`
    },
    {
        regex: new RegExp(`@PathVariable ${type} id\\)`, 'gm'),
        content: `@PathVariable ${convertType} id)`
    }
]

export const entityResourceTestNeedles = (camelCaseEntityName, type, convertType) => [
    {
        regex: getType(type),
        content: getValue(convertType)
    },
    {
        regex: getMethodReg(type),
        content: getMethod(convertType)
    },
    {
        regex: new RegExp(`${camelCaseEntityName}\\.setId\\(${getDefaultValue(type)}\\)`, 'gm'),
        content: `${camelCaseEntityName}.setId(${getDefaultValue(convertType)})`
    },
    {
        regex: setMethodReg(type),
        content: setMethod(convertType)
    },
    {
        regex: countMethodReg(type),
        content: countMethod(convertType)
    }
]

export const uuidRegex = '^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12};'

export const entityRepositoryNeedles = (type) => [
    `Relationships(${type} id)`, `@Param("id") ${type} id`
];

export const entityServiceNeedles = (type) => [
    `findOne(${type} id)`, `delete(${type} id)`
];
