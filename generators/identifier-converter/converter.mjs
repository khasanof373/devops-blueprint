/**
 * MIT License
 *
 * Copyright (c) 2023 Devops.uz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {entityTestNeedles} from "./needles.mjs";

/**
 * Replace the Entity id type in a file based on the given needles.
 *
 * @param generator The jhipster generator.
 * @param path The file to modify.
 * @param replaceNeedles Array of needles to replace.
 * @param convertObject convert object types
 */
export function replaceIdType(generator, path, replaceNeedles, convertObject) {
    replaceNeedles.forEach(needle => {
        generator.replaceContent(path, needle, needle.replace(convertObject.idType, convertObject.convertType), false);
    });
}

/**
 *
 * @param generator
 * @param entityPath
 * @param replaceNeedles
 */
export function replaceRegexNeedles(generator, entityPath, replaceNeedles) {
    replaceNeedles.forEach(needle => {
        generator.replaceContent(entityPath, needle.regex, needle.content);
    });
}

export function getDefaultValueConvertType(convertType) {
    if (convertType === 'UUID') {
        return 'UUID.randomUUID()';
    } else if (convertType === 'Long') {
        return 'Long.MAX_VALUE';
    } else if (convertType === 'Integer') {
        return 'Integer.MAX_VALUE';
    } else if (convertType === 'String') {
        return '"DEVOPS"';
    }
}

export function replaceIdAnnotations(generator, entityPath, entity, convertType, packageName) {
    if (convertType === 'UUID') {
        generator.replaceContent(entityPath, '.*@SequenceGenerator.*\n', '', true);
        insertUuidImport(generator, entityPath, `package ${packageName}.domain;`)
    }
    generator.replaceContent(entityPath, '@GeneratedValue.*', '@GeneratedValue', true)
}

export function replaceEntityTests(generator, name, paths, convertType) {
    if (convertType === 'UUID') {
        insertUuidImport(generator, paths.filePath, paths.packageName);
    }
    let entityVarName = camelize(name);
    const regexNeedles = entityTestNeedles(entityVarName);
    replaceRegexNeedles(generator, paths.filePath, regexNeedles);
}

export function addImports(generator, path, needle, convertType) {
    if (convertType === 'UUID') {
        insertUuidImport(generator, path, needle);
    }
}

export function addImportsV2(generator, path, needle, insertNeedle, convertType, eqType) {
    if (convertType === eqType) {
        insertImport(generator, path, needle, insertNeedle);
    }
}

function insertImport(generator, entityPath, needle, insertNeedle) {
    generator.replaceContent(entityPath, needle, insertNeedle);
}

function insertUuidImport(generator, entityPath, needle) {
    generator.replaceContent(entityPath, needle, `${needle}\n\n import java.util.UUID;`)
}

export function getRandom(max) {
    return Math.floor(Math.random() * max);
}

export function snakeCase(string) {
    return string.replace(/\W+/g, " ")
        .split(/ |\B(?=[A-Z])/)
        .map(word => word.toLowerCase())
        .join('_');
}

export function camelize(str) {
    return str
        .replace(/\s(.)/g, function ($1) {
            return $1.toUpperCase();
        })
        .replace(/\s/g, '')
        .replace(/^(.)/, function ($1) {
            return $1.toLowerCase();
        });
}
